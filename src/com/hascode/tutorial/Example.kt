package com.hascode.tutorial

import kotlin.reflect.KProperty

data class Book(val title:String, val price:Float)

class Foo {
    var bar : String by MyDelegate()
}

class MyDelegate() {
    operator fun getValue(ref:Any?, prop:KProperty<*>) : String {
        return "reference $ref, delegated property ${prop.name}"
    }

    operator fun setValue(ref: Any?, prop:KProperty<*>, value:String) {
        println("reference $ref delegate set property ${prop.name} to value $value")
    }
}

class LazySample {
    val name : String by lazy {
        println("computing value..")
        "foo"
    }
}

class App {
    fun run(){
        val books = listOf(Book("Some book", 23.50f), Book("Another Book", 13.45f))
        for(book in books)
            println("Book - title: ${book.title} has a price of ${book.price}")

        val nums = (1..20)
        for(num in nums) println(num)
        println("Max: ${nums.max()}")

        println("lengthOf .. ")
        println(lengthOf("foo"))
        println(lengthOf(listOf("foo", "bar", "baz")))
        println(lengthOf(Book("A book", 1.50f)))

        println("Delegates ..")
        val f = Foo()
        println(f.bar)
        f.bar = "FooBar"

        println("Lazy properties")
        val l = LazySample()
        l.name
        l.name
    }


    fun max(a: Int, b: Int) = if(a > b) a else b

    fun divide(a:Int, b:Int) :Int? {
        if(b==null)
            return null
        return a/b
    }

    fun lengthOf(obj: Any) : Int? {
        when(obj){
            is String ->  return obj.length
            is List<*> -> return obj.size
            else -> return null
        }
    }
}

fun main(args:Array<String>){
   App().run()
}